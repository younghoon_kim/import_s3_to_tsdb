import os
import sys
import traceback

import util.DBManager as DBM
from util import s3_util
import pandas as pd
import datetime
from util.config import  MAX_DATA_STORGE_DATE,DEVICE_TYPE_CD
from util.query import QUERY_INSERT_INTO_GEN3_TEMP,QUERY_INSERT_INTO_GEN3_HIST,CREATE_PMS_GEN3_TEMP,DROP_PMS_GEN3_TEMP
from util.query import QUERY_INSERT_INTO_GEN2_TEMP,QUERY_INSERT_INTO_GEN2_HIST,CREATE_PMS_GEN2_TEMP,DROP_PMS_GEN2_TEMP
import logging.config
import json

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

query_select = "select {0} from tb_opr_mon_state_{1}_hist "
query_where = "  where colec_dt >= '{0}' and colec_dt < '{1}' "
query_insert_to_temp=QUERY_INSERT_INTO_GEN3_TEMP
query_insert_to_hist=QUERY_INSERT_INTO_GEN3_HIST
query_delete = "delete from tb_opr_mon_state_{0}_hist "

def set_logger():
    config = json.load(open('util/logger-config.json'))
    logging.config.dictConfig(config)
    global logger
    logger = logging.getLogger(__name__)



def execute_migrate_gen3(st_date_of_migrate_data,device_type,db_manager):
    try:
        db_manager.execute_commit(DROP_PMS_GEN3_TEMP)
        db_manager.execute_commit(CREATE_PMS_GEN3_TEMP)
        for i in range(MAX_DATA_STORGE_DATE):
        #for i in range(1):
            start_time = datetime.datetime.utcnow()

            logger.debug('=============1 day start : {0}============'.format(st_date_of_migrate_data))
            for i in range(24):
                # 1. truncate temp
                db_manager.execute_commit("truncate table tb_opr_mon_state_pms_gen3_hist_temp")
                # 2. date range to string
                query_st_time_str = (st_date_of_migrate_data + datetime.timedelta(hours=i)).strftime(
                    "%Y/%m/%d %H:%M:%S")
                query_end_time_str = (st_date_of_migrate_data + datetime.timedelta(hours=i+1)).strftime(
                    "%Y/%m/%d %H:%M:%S")
                # 3. start migration
                logger.debug('============= 1hour start : {0}~{1}============'.format(query_st_time_str,query_end_time_str))

                # 4.
                # 4-1 (copy)insert to temp
                db_manager.execute_commit(QUERY_INSERT_INTO_GEN3_TEMP+query_where.format(query_st_time_str,query_end_time_str))
                # 4-2 delete from original hist
                db_manager.execute_commit(query_delete.format(device_type) + query_where.format(query_st_time_str,query_end_time_str))
                # 4-3 insert back into hist
                db_manager.execute_commit(QUERY_INSERT_INTO_GEN3_HIST)
                logger.debug('=============1 hour done : {0}~{1}============'.format(query_st_time_str,query_end_time_str))

            logger.debug('end time : {0}'.format(datetime.datetime.utcnow() - start_time))
            st_date_of_migrate_data=st_date_of_migrate_data+datetime.timedelta(days=1)
    except Exception as e:
        logger.error(traceback.format_exc())


def execute_migrate_gen2(st_date_of_migrate_data,device_type,db_manager):
    try:
        db_manager.execute_commit(DROP_PMS_GEN2_TEMP)
        db_manager.execute_commit(CREATE_PMS_GEN2_TEMP)
        for i in range(MAX_DATA_STORGE_DATE):
        #for i in range(1):
            start_time = datetime.datetime.utcnow()

            logger.debug('=============1 day start : {0}============'.format(st_date_of_migrate_data))
            for i in range(24):
                # 1. truncate temp
                db_manager.execute_commit("truncate table tb_opr_mon_state_pms_gen2_hist_temp")
                # 2. date range to string
                query_st_time_str = (st_date_of_migrate_data + datetime.timedelta(hours=i)).strftime(
                    "%Y/%m/%d %H:%M:%S")
                query_end_time_str = (st_date_of_migrate_data + datetime.timedelta(hours=i+1)).strftime(
                    "%Y/%m/%d %H:%M:%S")
                # 3. start migration
                logger.debug('============= 1hour start : {0}~{1}============'.format(query_st_time_str,query_end_time_str))

                # 4.
                # 4-1 (copy)insert to temp
                db_manager.execute_commit(QUERY_INSERT_INTO_GEN2_TEMP+query_where.format(query_st_time_str,query_end_time_str))
                # 4-2 delete from original hist
                db_manager.execute_commit(query_delete.format(device_type) + query_where.format(query_st_time_str,query_end_time_str))
                # 4-3 insert back into hist
                db_manager.execute_commit(QUERY_INSERT_INTO_GEN2_HIST)
                logger.debug('=============1 hour done : {0}~{1}============'.format(query_st_time_str,query_end_time_str))

            logger.debug('end time : {0}'.format(datetime.datetime.utcnow() - start_time))
            st_date_of_migrate_data=st_date_of_migrate_data+datetime.timedelta(days=1)
    except Exception as e:
        logger.error(traceback.format_exc())


def migrate_data(device_type):
    # 0-0 logfile,db_manager,device_type 설정
    set_logger()
    db_manager = DBM.DBManager()
    # 0-1 날짜 설정
    start_time = datetime.datetime.utcnow()
    #st_date_of_migrate_data = start_time-datetime.timedelta(days=MAX_DATA_STORGE_DATE)
    st_date_of_migrate_data = start_time - datetime.timedelta(days=10)
    # 0-2 쿼리에 넣을 날짜 문자열 생성
    st_date_trunc_to_hour=st_date_of_migrate_data.replace(hour=0,minute=0, second=0, microsecond=0)
    # 이동 실행
    logger.debug('=====start migration : start_day = {0} , start_time {1}'.format(st_date_trunc_to_hour,start_time))
    print('=====start migration : target_day = {0} '.format(start_time))

    # 1. device type 입력 및 이관함수 실행
    if device_type == '2':
        device_type = DEVICE_TYPE_CD.get("gen2")
        execute_migrate_gen2(st_date_trunc_to_hour, device_type, db_manager)
    elif device_type == '3':
        device_type = DEVICE_TYPE_CD.get("gen3")
        execute_migrate_gen3(st_date_trunc_to_hour, device_type, db_manager)
    else:
        print("choose 2 or 3 ")
        return
    logger.debug('end time : {0}'.format(datetime.datetime.utcnow() - start_time))
    logger.debug('=============end============')
    print('=====start migration : target_day = {0} '.format(datetime.datetime.utcnow() - start_time))




if __name__ == '__main__':
    migrate_data(sys.argv[1])


