import traceback
# from multipledispatch import dispatch
from collections import Iterable
import psycopg2
from psycopg2.extras import RealDictCursor
from util.config import HOST, PORT, DBNAME, USER, PWD,PATH


class DBManager:
    def __init__(self):
        self.conn = self.__connect()

    def teardown(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def __connect(self):
        print("database conneting....")
        return psycopg2.connect(host=HOST, port=PORT, dbname=DBNAME, user=USER, password=PWD,options=PATH)

    def __read_query(self, query_filename):
        query = ''
        with open(query_filename, 'r') as f:
            query = f.read()

        return query

    def execute_select(self, query):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query)
            result = cur.fetchall()
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()

            return None

        cur.close()
        return result

    # @dispatch(str)
    def execute_commit(self, query):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query)
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()
            self.conn.rollback()

        cur.close()
        self.conn.commit()

    # @dispatch(str, Iterable)
    def execute_commit_with_data(self, query, data):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query, data)
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()
            self.conn.rollback()

        cur.close()
        self.conn.commit()

    def execute_commit_with_data_list(self, query, data_list):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            for data in data_list:
                cur.execute(query,  data)
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()
            self.conn.rollback()

        cur.close()
        self.conn.commit()

    def copy_to_file(self, query, file_name):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor()
        SQL_for_file_output = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
        print(SQL_for_file_output)
        try:
            with open(file_name, 'w') as f_output:
                cur.copy_expert(SQL_for_file_output, f_output)

        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()

            return -1

        rowcount=cur.rowcount
        cur.close()
        print(rowcount)
        return rowcount

    def copy_csv_to_db(self,query,file_name):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor()
        SQL_for_file_input = "COPY ({0}) FROM stdin DELIMITER ',' CSV HEADER".format(query)
        print(SQL_for_file_input)
        try:
            with open(file_name, 'r') as f_input:
                cur.copy_expert(SQL_for_file_input, f_input)
                print(f_input.name)

        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()

            return -1

        rowcount=cur.rowcount
        cur.close()
        print(rowcount+" rows inserted")
        return rowcount

