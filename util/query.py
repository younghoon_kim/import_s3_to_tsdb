#****************************************************
#****************************GEN3******************************
#****************************************************
DROP_PMS_GEN3_TEMP="drop table if exists tb_opr_mon_state_pms_gen3_hist_temp;"
CREATE_PMS_GEN3_TEMP = '''
create table tb_opr_mon_state_pms_gen3_hist_temp
(
    device_id varchar not null,
--     device_type_cd varchar(4) not null,
    oper_stus_cd varchar,
    --EMS INFO
    dynamic_opt_flag numeric,
    load_pw numeric,
    load_pwh_day numeric,
    load_pwh_day_delta numeric,
    core_pw numeric,
    core_pwh_day numeric,
    core_pwh_day_delta numeric,
    grid_pur_pwh_day numeric,
    grid_pur_pwh_day_delta numeric,
    grid_fdin_pwh_day numeric,
    grid_fdin_pwh_day_delta numeric,
    energy_policy varchar(5),
    grid_st_cd numeric,
    smtr_st_cd numeric,
    meter_v numeric,
    meter_i numeric,
    meter_freq numeric,
    pcs_ctrl_cmd1 varchar,
    pcs_ctrl_cmd2 numeric,
    pv_pwh_day numeric,
    pv_pwh_day_delta numeric,
    bt_chrg_pwh_day numeric,
    bt_chrg_pwh_day_delta numeric,
    bt_dchrg_pwh_day numeric,
    bt_dchrg_pwh_day_delta numeric,
    pcs_fd_pwh_day numeric,
    pcs_fd_pwh_day_delta numeric,
    pcs_pch_pwh_day numeric,
    pcs_pch_pwh_day_delta numeric,
    --PCS INFO
    pcs_st_cd numeric,
    pcs_opmode1 varchar,
    pcs_opmode2 numeric,
    pv_avail_flag numeric,
    pv_stus_cd numeric,
    pv_pw numeric,
    pv_v1 numeric,
    pv_i1 numeric,
    pv_pw1 numeric,
    pv_v2 numeric,
    pv_i2 numeric,
    pv_pw2 numeric,
    pv_v3 numeric,
    pv_i3 numeric,
    pv_pw3 numeric,
    bt_chrg_avail_flag numeric,
    bt_dchrg_avail_flag numeric,
    bt_stus_cd numeric,
    bt_pw numeric,
    bt_v numeric,
    bt_i numeric,
    inverter_v numeric,
    inverter_i numeric,
    inverter_pw numeric,
    inverter_freq numeric,
    dc_link_v numeric,
    grid_v numeric,
    grid_i numeric,
    grid_active_pw numeric,
    grid_reactive_pw numeric,
    grid_pwfactor numeric,
    grid_freq numeric,
    inverter_tgt_pw numeric,
    battery_tgt_pw numeric,
    pv_limit_pw numeric,
    pw_ctrl_pnt numeric,
    --Battery INFO
    rack_cnt numeric,
    real_soc_avg numeric,
    user_soc_avg numeric,
    --Cellinfo
    rack_id1 numeric,
    rack_v1 numeric,
    rack_i1 numeric,
    soc1 numeric,
    soh1 numeric,
    cell_max_v1 numeric,
    cell_max_v_module_id1 numeric,
    cell_max_v_cell_id1 numeric,
    cell_min_v1 numeric,
    cell_min_v_module_id1 numeric,
    cell_min_v_cell_id1 numeric,
    cell_avg_v1 numeric,
    cell_max_t1 numeric,
    cell_max_t_module_id1 numeric,
    cell_max_t_cell_id1 numeric,
    cell_min_t1 numeric,
    cell_min_t_module_id1 numeric,
    cell_min_t_cell_id1 numeric,
    cell_avg_t1 numeric,
    rack_id2 numeric,
    rack_v2 numeric,
    rack_i2 numeric,
    soc2 numeric,
    soh2 numeric,
    cell_max_v2 numeric,
    cell_max_v_module_id2 numeric,
    cell_max_v_cell_id2 numeric,
    cell_min_v2 numeric,
    cell_min_v_module_id2 numeric,
    cell_min_v_cell_id2 numeric,
    cell_avg_v2 numeric,
    cell_max_t2 numeric,
    cell_max_t_module_id2 numeric,
    cell_max_t_cell_id2 numeric,
    cell_min_t2 numeric,
    cell_min_t_module_id2 numeric,
    cell_min_t_cell_id2 numeric,
    cell_avg_t2 numeric,
    rack_id3 numeric,
    rack_v3 numeric,
    rack_i3 numeric,
    soc3 numeric,
    soh3 numeric,
    cell_max_v3 numeric,
    cell_max_v_module_id3 numeric,
    cell_max_v_cell_id3 numeric,
    cell_min_v3 numeric,
    cell_min_v_module_id3 numeric,
    cell_min_v_cell_id3 numeric,
    cell_avg_v3 numeric,
    cell_max_t3 numeric,
    cell_max_t_module_id3 numeric,
    cell_max_t_cell_id3 numeric,
    cell_min_t3 numeric,
    cell_min_t_module_id3 numeric,
    cell_min_t_cell_id3 numeric,
    cell_avg_t3 numeric,
    --Error INFO
    error_codes varchar,
    colec_dt timestamp with time zone,
    create_dt timestamp with time zone default now() not null,
    meter_active_pw numeric,
    meter_reactive_pw numeric,
    meter_pwfactor numeric,
    pcs_grid_i numeric,
    pcs_grid_v numeric,
    pcs_grid_active_pw numeric,
    pcs_grid_reactive_pw numeric,
    pcs_grid_pwfactor numeric,
    pcs_grid_freq numeric,
constraint pk_opr_mon_state_pms_gen3_hist_temp
  primary key (device_id,colec_dt)
);
'''

QUERY_INSERT_INTO_GEN3_TEMP="insert into tb_opr_mon_state_pms_gen3_hist_temp(\
device_id,\
oper_stus_cd,\
dynamic_opt_flag,\
load_pw,\
load_pwh_day,\
load_pwh_day_delta,\
core_pw,\
core_pwh_day,\
core_pwh_day_delta,\
grid_pur_pwh_day,\
grid_pur_pwh_day_delta,\
grid_fdin_pwh_day,\
grid_fdin_pwh_day_delta,\
energy_policy,\
grid_st_cd,\
smtr_st_cd,\
meter_v,\
meter_i,\
meter_freq,\
pcs_ctrl_cmd1,\
pcs_ctrl_cmd2,\
pv_pwh_day,\
pv_pwh_day_delta,\
bt_chrg_pwh_day,\
bt_chrg_pwh_day_delta,\
bt_dchrg_pwh_day,\
bt_dchrg_pwh_day_delta,\
pcs_fd_pwh_day,\
pcs_fd_pwh_day_delta,\
pcs_pch_pwh_day,\
pcs_pch_pwh_day_delta,\
pcs_st_cd,\
pcs_opmode1,\
pcs_opmode2,\
pv_avail_flag,\
pv_stus_cd,\
pv_pw,\
pv_v1,\
pv_i1,\
pv_pw1,\
pv_v2,\
pv_i2,\
pv_pw2,\
pv_v3,\
pv_i3,\
pv_pw3,\
bt_chrg_avail_flag,\
bt_dchrg_avail_flag,\
bt_stus_cd,\
bt_pw,\
bt_v,\
bt_i,\
inverter_v,\
inverter_i,\
inverter_pw,\
inverter_freq,\
dc_link_v,\
inverter_tgt_pw,\
battery_tgt_pw,\
pv_limit_pw,\
pw_ctrl_pnt,\
rack_cnt,\
real_soc_avg,\
user_soc_avg,\
rack_id1,\
rack_v1,\
rack_i1,\
soc1,\
soh1,\
cell_max_v1,\
cell_max_v_module_id1,\
cell_max_v_cell_id1,\
cell_min_v1,\
cell_min_v_module_id1,\
cell_min_v_cell_id1,\
cell_avg_v1,\
cell_max_t1,\
cell_max_t_module_id1,\
cell_max_t_cell_id1,\
cell_min_t1,\
cell_min_t_module_id1,\
cell_min_t_cell_id1,\
cell_avg_t1,\
rack_id2,\
rack_v2,\
rack_i2,\
soc2,\
soh2,\
cell_max_v2,\
cell_max_v_module_id2,\
cell_max_v_cell_id2,\
cell_min_v2,\
cell_min_v_module_id2,\
cell_min_v_cell_id2,\
cell_avg_v2,\
cell_max_t2,\
cell_max_t_module_id2,\
cell_max_t_cell_id2,\
cell_min_t2,\
cell_min_t_module_id2,\
cell_min_t_cell_id2,\
cell_avg_t2,\
rack_id3,\
rack_v3,\
rack_i3,\
soc3,\
soh3,\
cell_max_v3,\
cell_max_v_module_id3,\
cell_max_v_cell_id3,\
cell_min_v3,\
cell_min_v_module_id3,\
cell_min_v_cell_id3,\
cell_avg_v3,\
cell_max_t3,\
cell_max_t_module_id3,\
cell_max_t_cell_id3,\
cell_min_t3,\
cell_min_t_module_id3,\
cell_min_t_cell_id3,\
cell_avg_t3,\
error_codes,\
colec_dt,\
create_dt,\
meter_active_pw,\
meter_reactive_pw,\
meter_pwfactor,\
pcs_grid_v,\
pcs_grid_i,\
pcs_grid_active_pw,\
pcs_grid_reactive_pw,\
pcs_grid_pwfactor,\
pcs_grid_freq\
) select device_id,\
oper_stus_cd,\
dynamic_opt_flag,\
load_pw,\
load_pwh_day,\
load_pwh_day_delta,\
core_pw,\
core_pwh_day,\
core_pwh_day_delta,\
grid_pur_pwh_day,\
grid_pur_pwh_day_delta,\
grid_fdin_pwh_day,\
grid_fdin_pwh_day_delta,\
energy_policy,\
grid_st_cd,\
smtr_st_cd,\
meter_v,\
meter_i,\
meter_freq,\
pcs_ctrl_cmd1,\
pcs_ctrl_cmd2,\
pv_pwh_day,\
pv_pwh_day_delta,\
bt_chrg_pwh_day,\
bt_chrg_pwh_day_delta,\
bt_dchrg_pwh_day,\
bt_dchrg_pwh_day_delta,\
pcs_fd_pwh_day,\
pcs_fd_pwh_day_delta,\
pcs_pch_pwh_day,\
pcs_pch_pwh_day_delta,\
pcs_st_cd,\
pcs_opmode1,\
pcs_opmode2,\
pv_avail_flag,\
pv_stus_cd,\
pv_pw,\
pv_v1,\
pv_i1,\
pv_pw1,\
pv_v2,\
pv_i2,\
pv_pw2,\
pv_v3,\
pv_i3,\
pv_pw3,\
bt_chrg_avail_flag,\
bt_dchrg_avail_flag,\
bt_stus_cd,\
bt_pw,\
bt_v,\
bt_i,\
inverter_v,\
inverter_i,\
inverter_pw,\
inverter_freq,\
dc_link_v,\
inverter_tgt_pw,\
battery_tgt_pw,\
pv_limit_pw,\
pw_ctrl_pnt,\
rack_cnt,\
real_soc_avg,\
user_soc_avg,\
rack_id1,\
rack_v1,\
rack_i1,\
soc1,\
soh1,\
cell_max_v1,\
cell_max_v_module_id1,\
cell_max_v_cell_id1,\
cell_min_v1,\
cell_min_v_module_id1,\
cell_min_v_cell_id1,\
cell_avg_v1,\
cell_max_t1,\
cell_max_t_module_id1,\
cell_max_t_cell_id1,\
cell_min_t1,\
cell_min_t_module_id1,\
cell_min_t_cell_id1,\
cell_avg_t1,\
rack_id2,\
rack_v2,\
rack_i2,\
soc2,\
soh2,\
cell_max_v2,\
cell_max_v_module_id2,\
cell_max_v_cell_id2,\
cell_min_v2,\
cell_min_v_module_id2,\
cell_min_v_cell_id2,\
cell_avg_v2,\
cell_max_t2,\
cell_max_t_module_id2,\
cell_max_t_cell_id2,\
cell_min_t2,\
cell_min_t_module_id2,\
cell_min_t_cell_id2,\
cell_avg_t2,\
rack_id3,\
rack_v3,\
rack_i3,\
soc3,\
soh3,\
cell_max_v3,\
cell_max_v_module_id3,\
cell_max_v_cell_id3,\
cell_min_v3,\
cell_min_v_module_id3,\
cell_min_v_cell_id3,\
cell_avg_v3,\
cell_max_t3,\
cell_max_t_module_id3,\
cell_max_t_cell_id3,\
cell_min_t3,\
cell_min_t_module_id3,\
cell_min_t_cell_id3,\
cell_avg_t3,\
error_codes,\
colec_dt,\
create_dt,\
meter_active_pw,\
meter_reactive_pw,\
meter_pwfactor,\
grid_v as pcs_grid_v,\
grid_i as pcs_grid_i,\
grid_active_pw as pcs_grid_active_pw,\
grid_reactive_pw as pcs_grid_reactive_pw,\
grid_pwfactor as pcs_grid_pwfactor,\
grid_freq as pcs_grid_freq from tb_opr_mon_state_pms_gen3_hist"

QUERY_INSERT_INTO_GEN3_HIST="insert into tb_opr_mon_state_pms_gen3_hist(\
                                           device_id,\
oper_stus_cd,\
dynamic_opt_flag,\
load_pw,\
load_pwh_day,\
load_pwh_day_delta,\
core_pw,\
core_pwh_day,\
core_pwh_day_delta,\
grid_pur_pwh_day,\
grid_pur_pwh_day_delta,\
grid_fdin_pwh_day,\
grid_fdin_pwh_day_delta,\
energy_policy,\
grid_st_cd,\
smtr_st_cd,\
meter_v,\
meter_i,\
meter_freq,\
pcs_ctrl_cmd1,\
pcs_ctrl_cmd2,\
pv_pwh_day,\
pv_pwh_day_delta,\
bt_chrg_pwh_day,\
bt_chrg_pwh_day_delta,\
bt_dchrg_pwh_day,\
bt_dchrg_pwh_day_delta,\
pcs_fd_pwh_day,\
pcs_fd_pwh_day_delta,\
pcs_pch_pwh_day,\
pcs_pch_pwh_day_delta,\
pcs_st_cd,\
pcs_opmode1,\
pcs_opmode2,\
pv_avail_flag,\
pv_stus_cd,\
pv_pw,\
pv_v1,\
pv_i1,\
pv_pw1,\
pv_v2,\
pv_i2,\
pv_pw2,\
pv_v3,\
pv_i3,\
pv_pw3,\
bt_chrg_avail_flag,\
bt_dchrg_avail_flag,\
bt_stus_cd,\
bt_pw,\
bt_v,\
bt_i,\
inverter_v,\
inverter_i,\
inverter_pw,\
inverter_freq,\
dc_link_v,\
inverter_tgt_pw,\
battery_tgt_pw,\
pv_limit_pw,\
pw_ctrl_pnt,\
rack_cnt,\
real_soc_avg,\
user_soc_avg,\
rack_id1,\
rack_v1,\
rack_i1,\
soc1,\
soh1,\
cell_max_v1,\
cell_max_v_module_id1,\
cell_max_v_cell_id1,\
cell_min_v1,\
cell_min_v_module_id1,\
cell_min_v_cell_id1,\
cell_avg_v1,\
cell_max_t1,\
cell_max_t_module_id1,\
cell_max_t_cell_id1,\
cell_min_t1,\
cell_min_t_module_id1,\
cell_min_t_cell_id1,\
cell_avg_t1,\
rack_id2,\
rack_v2,\
rack_i2,\
soc2,\
soh2,\
cell_max_v2,\
cell_max_v_module_id2,\
cell_max_v_cell_id2,\
cell_min_v2,\
cell_min_v_module_id2,\
cell_min_v_cell_id2,\
cell_avg_v2,\
cell_max_t2,\
cell_max_t_module_id2,\
cell_max_t_cell_id2,\
cell_min_t2,\
cell_min_t_module_id2,\
cell_min_t_cell_id2,\
cell_avg_t2,\
rack_id3,\
rack_v3,\
rack_i3,\
soc3,\
soh3,\
cell_max_v3,\
cell_max_v_module_id3,\
cell_max_v_cell_id3,\
cell_min_v3,\
cell_min_v_module_id3,\
cell_min_v_cell_id3,\
cell_avg_v3,\
cell_max_t3,\
cell_max_t_module_id3,\
cell_max_t_cell_id3,\
cell_min_t3,\
cell_min_t_module_id3,\
cell_min_t_cell_id3,\
cell_avg_t3,\
error_codes,\
colec_dt,\
create_dt,\
meter_active_pw,\
meter_reactive_pw,\
meter_pwfactor,\
pcs_grid_i,\
pcs_grid_v,\
pcs_grid_active_pw,\
pcs_grid_reactive_pw,\
pcs_grid_pwfactor,\
pcs_grid_freq) select device_id,\
oper_stus_cd,\
dynamic_opt_flag,\
load_pw,\
load_pwh_day,\
load_pwh_day_delta,\
core_pw,\
core_pwh_day,\
core_pwh_day_delta,\
grid_pur_pwh_day,\
grid_pur_pwh_day_delta,\
grid_fdin_pwh_day,\
grid_fdin_pwh_day_delta,\
energy_policy,\
grid_st_cd,\
smtr_st_cd,\
meter_v,\
meter_i,\
meter_freq,\
pcs_ctrl_cmd1,\
pcs_ctrl_cmd2,\
pv_pwh_day,\
pv_pwh_day_delta,\
bt_chrg_pwh_day,\
bt_chrg_pwh_day_delta,\
bt_dchrg_pwh_day,\
bt_dchrg_pwh_day_delta,\
pcs_fd_pwh_day,\
pcs_fd_pwh_day_delta,\
pcs_pch_pwh_day,\
pcs_pch_pwh_day_delta,\
pcs_st_cd,\
pcs_opmode1,\
pcs_opmode2,\
pv_avail_flag,\
pv_stus_cd,\
pv_pw,\
pv_v1,\
pv_i1,\
pv_pw1,\
pv_v2,\
pv_i2,\
pv_pw2,\
pv_v3,\
pv_i3,\
pv_pw3,\
bt_chrg_avail_flag,\
bt_dchrg_avail_flag,\
bt_stus_cd,\
bt_pw,\
bt_v,\
bt_i,\
inverter_v,\
inverter_i,\
inverter_pw,\
inverter_freq,\
dc_link_v,\
inverter_tgt_pw,\
battery_tgt_pw,\
pv_limit_pw,\
pw_ctrl_pnt,\
rack_cnt,\
real_soc_avg,\
user_soc_avg,\
rack_id1,\
rack_v1,\
rack_i1,\
soc1,\
soh1,\
cell_max_v1,\
cell_max_v_module_id1,\
cell_max_v_cell_id1,\
cell_min_v1,\
cell_min_v_module_id1,\
cell_min_v_cell_id1,\
cell_avg_v1,\
cell_max_t1,\
cell_max_t_module_id1,\
cell_max_t_cell_id1,\
cell_min_t1,\
cell_min_t_module_id1,\
cell_min_t_cell_id1,\
cell_avg_t1,\
rack_id2,\
rack_v2,\
rack_i2,\
soc2,\
soh2,\
cell_max_v2,\
cell_max_v_module_id2,\
cell_max_v_cell_id2,\
cell_min_v2,\
cell_min_v_module_id2,\
cell_min_v_cell_id2,\
cell_avg_v2,\
cell_max_t2,\
cell_max_t_module_id2,\
cell_max_t_cell_id2,\
cell_min_t2,\
cell_min_t_module_id2,\
cell_min_t_cell_id2,\
cell_avg_t2,\
rack_id3,\
rack_v3,\
rack_i3,\
soc3,\
soh3,\
cell_max_v3,\
cell_max_v_module_id3,\
cell_max_v_cell_id3,\
cell_min_v3,\
cell_min_v_module_id3,\
cell_min_v_cell_id3,\
cell_avg_v3,\
cell_max_t3,\
cell_max_t_module_id3,\
cell_max_t_cell_id3,\
cell_min_t3,\
cell_min_t_module_id3,\
cell_min_t_cell_id3,\
cell_avg_t3,\
error_codes,\
colec_dt,\
create_dt,\
meter_active_pw,\
meter_reactive_pw,\
meter_pwfactor,\
pcs_grid_i,\
pcs_grid_v,\
pcs_grid_active_pw,\
pcs_grid_reactive_pw,\
pcs_grid_pwfactor,\
pcs_grid_freq from tb_opr_mon_state_pms_gen3_hist_temp "
#****************************************************
# *********************GEN2***********************
#****************************************************

DROP_PMS_GEN2_TEMP="drop table if exists tb_opr_mon_state_pms_gen2_hist_temp;"
CREATE_PMS_GEN2_TEMP = '''
create table tb_opr_mon_state_pms_gen2_hist_temp
(
	device_id varchar not null,
-- 	device_type_cd varchar not null,
	oper_stus_cd varchar,
	pv_pw numeric,
	pv_pw_h numeric,
	pv_pw_co2 numeric,
	pv_stus_cd varchar,
	cons_pw numeric,
	cons_pw_h numeric,
	cons_pw_co2 numeric,
	bt_pw numeric,
	bt_chrg_pw_h numeric,
	bt_chrg_pw_co2 numeric,
	bt_dchrg_pw_h numeric,
	bt_dchrg_pw_co2 numeric,
	bt_soc numeric,
	bt_soh numeric,
	bt_stus_cd varchar,
	grid_pw numeric,
	grid_ob_pw_h numeric,
	grid_ob_pw_co2 numeric,
	grid_tr_pw_h numeric,
	grid_tr_pw_co2 numeric,
	grid_stus_cd varchar,
	pcs_pw numeric,
	pcs_fd_pw_h numeric,
	pcs_pch_pw_h numeric,
	ems_opmode varchar,
	pcs_opmode1 varchar,
	pcs_opmode2 varchar,
	pcs_tgt_pw numeric,
	feed_in_limit varchar,
	max_inverter_pw_cd varchar,
	rfsh_prd numeric default 60 not null,
	outlet_pw numeric,
	outlet_pw_h numeric,
	outlet_pw_co2 numeric,
	pwr_range_cd_ess_grid varchar,
	pwr_range_cd_grid_ess varchar,
	pwr_range_cd_grid_load varchar,
	pwr_range_cd_ess_load varchar,
	pwr_range_cd_pv_ess varchar,
	pcs_comm_err_rate numeric,
	smtr_comm_err_rate numeric,
	m2m_comm_err_rate numeric,
	pcs_flag0 varchar,
	pcs_flag1 varchar,
	pcs_flag2 varchar,
	pcs_opmode_cmd1 varchar,
	pcs_opmode_cmd2 varchar,
	pv_v1 numeric,
	pv_i1 numeric,
	pv_pw1 numeric,
	pv_v2 numeric,
	pv_i2 numeric,
	pv_pw2 numeric,
	inverter_v numeric,
	inverter_i numeric,
	inverter_pw numeric,
	dc_link_v numeric,
	g_rly_cnt numeric,
	bat_rly_cnt numeric,
	grid_to_grid_rly_cnt numeric,
	bat_pchrg_rly_cnt numeric,
	bms_flag0 varchar,
	bms_diag0 varchar,
	rack_v numeric,
	rack_i numeric,
	cell_max_v numeric,
	cell_min_v numeric,
	cell_avg_v numeric,
	cell_max_t numeric,
	cell_min_t numeric,
	cell_avg_t numeric,
	tray_cnt numeric,
	smtr_tp_cd varchar,
	smtr_pulse_cnt numeric,
	smtr_modl_cd varchar,
	pv_max_pwr1 numeric,
	pv_max_pwr2 numeric,
	instl_rgn varchar,
	mmtr_slv_addr numeric,
	basicmode_cd varchar,
	sys_st_cd varchar,
	bt_chrg_st_cd varchar,
	bt_dchrg_st_cd varchar,
	pv_st_cd varchar,
	grid_st_cd varchar,
	smtr_st_cd varchar,
	smtr_tr_cnter numeric,
	smtr_ob_cnter numeric,
	dn_enable varchar,
	dc_start varchar,
	dc_end varchar,
	nc_start varchar,
	nc_end varchar,
	grid_code0 varchar,
	grid_code1 varchar,
	grid_code2 varchar,
	pcon_bat_targetpower varchar,
	grid_code3 varchar,
	bdc_module_code0 varchar,
	bdc_module_code1 varchar,
	bdc_module_code2 varchar,
	bdc_module_code3 varchar,
	fault5_realtime_year varchar,
	fault5_realtime_month varchar,
	fault5_day varchar,
	fault5_hour varchar,
	fault5_minute varchar,
	fault5_second varchar,
	fault5_datahigh varchar,
	fault5_datalow varchar,
	fault6_realtime_year varchar,
	fault6_realtime_month varchar,
	fault6_realtime_day varchar,
	fault6_realtime_hour varchar,
	fault6_realtime_minute varchar,
	fault6_realtime_second varchar,
	fault6_datahigh varchar,
	fault6_datalow varchar,
	fault7_realtime_year varchar,
	fault7_realtime_month varchar,
	fault7_realtime_day varchar,
	fault7_realtime_hour varchar,
	fault7_realtime_minute varchar,
	fault7_realtime_second varchar,
	fault7_datahigh varchar,
	fault7_datalow varchar,
	fault8_realtime_year varchar,
	fault8_realtime_month varchar,
	fault8_realtime_day varchar,
	fault8_realtime_hour varchar,
	fault8_realtime_minute varchar,
	fault8_realtime_second varchar,
	fault8_datahigh varchar,
	fault8_datalow varchar,
	fault9_realtime_year varchar,
	fault9_realtime_month varchar,
	fault9_realtime_day varchar,
	fault9_realtime_hour varchar,
	fault9_realtime_minute varchar,
	fault9_realtime_second varchar,
	fault9_datahigh varchar,
	fault9_datalow varchar,
--	is_connect boolean default false,
	bt_real_soc numeric,
	load_main_pw numeric,
	load_sub_pw numeric,
	load_main_pw_h numeric,
	load_sub_pw_h numeric,
	inverter_freq numeric,
	bt_v numeric,
	bt_i numeric,
	create_dt timestamp with time zone default now() not null,
	colec_dt timestamp with time zone not null,
    meter_v numeric,
    meter_i numeric,
    meter_active_pw numeric,
    meter_reactive_pw numeric,
    meter_pwfactor numeric,
    meter_freq numeric,
	constraint pk_opr_mon_state_pms_gen2_hist_temp primary key (device_id,colec_dt)
);
'''

QUERY_INSERT_INTO_GEN2_TEMP='''
insert into tb_opr_mon_state_pms_gen2_hist_temp(
	device_id varchar not null,
-- 	device_type_cd varchar not null,
	oper_stus_cd varchar,
	pv_pw numeric,
	pv_pw_h numeric,
	pv_pw_co2 numeric,
	pv_stus_cd varchar,
	cons_pw numeric,
	cons_pw_h numeric,
	cons_pw_co2 numeric,
	bt_pw numeric,
	bt_chrg_pw_h numeric,
	bt_chrg_pw_co2 numeric,
	bt_dchrg_pw_h numeric,
	bt_dchrg_pw_co2 numeric,
	bt_soc numeric,
	bt_soh numeric,
	bt_stus_cd varchar,
	grid_pw numeric,
	grid_ob_pw_h numeric,
	grid_ob_pw_co2 numeric,
	grid_tr_pw_h numeric,
	grid_tr_pw_co2 numeric,
	grid_stus_cd varchar,
	pcs_pw numeric,
	pcs_fd_pw_h numeric,
	pcs_pch_pw_h numeric,
	ems_opmode varchar,
	pcs_opmode1 varchar,
	pcs_opmode2 varchar,
	pcs_tgt_pw numeric,
	feed_in_limit varchar,
	max_inverter_pw_cd varchar,
	rfsh_prd numeric default 60 not null,
	outlet_pw numeric,
	outlet_pw_h numeric,
	outlet_pw_co2 numeric,
	pwr_range_cd_ess_grid varchar,
	pwr_range_cd_grid_ess varchar,
	pwr_range_cd_grid_load varchar,
	pwr_range_cd_ess_load varchar,
	pwr_range_cd_pv_ess varchar,
	pcs_comm_err_rate numeric,
	smtr_comm_err_rate numeric,
	m2m_comm_err_rate numeric,
	pcs_flag0 varchar,
	pcs_flag1 varchar,
	pcs_flag2 varchar,
	pcs_opmode_cmd1 varchar,
	pcs_opmode_cmd2 varchar,
	pv_v1 numeric,
	pv_i1 numeric,
	pv_pw1 numeric,
	pv_v2 numeric,
	pv_i2 numeric,
	pv_pw2 numeric,
	inverter_v numeric,
	inverter_i numeric,
	inverter_pw numeric,
	dc_link_v numeric,
	g_rly_cnt numeric,
	bat_rly_cnt numeric,
	grid_to_grid_rly_cnt numeric,
	bat_pchrg_rly_cnt numeric,
	bms_flag0 varchar,
	bms_diag0 varchar,
	rack_v numeric,
	rack_i numeric,
	cell_max_v numeric,
	cell_min_v numeric,
	cell_avg_v numeric,
	cell_max_t numeric,
	cell_min_t numeric,
	cell_avg_t numeric,
	tray_cnt numeric,
	smtr_tp_cd varchar,
	smtr_pulse_cnt numeric,
	smtr_modl_cd varchar,
	pv_max_pwr1 numeric,
	pv_max_pwr2 numeric,
	instl_rgn varchar,
	mmtr_slv_addr numeric,
	basicmode_cd varchar,
	sys_st_cd varchar,
	bt_chrg_st_cd varchar,
	bt_dchrg_st_cd varchar,
	pv_st_cd varchar,
	grid_st_cd varchar,
	smtr_st_cd varchar,
	smtr_tr_cnter numeric,
	smtr_ob_cnter numeric,
	dn_enable varchar,
	dc_start varchar,
	dc_end varchar,
	nc_start varchar,
	nc_end varchar,
	grid_code0 varchar,
	grid_code1 varchar,
	grid_code2 varchar,
	pcon_bat_targetpower varchar,
	grid_code3 varchar,
	bdc_module_code0 varchar,
	bdc_module_code1 varchar,
	bdc_module_code2 varchar,
	bdc_module_code3 varchar,
	fault5_realtime_year varchar,
	fault5_realtime_month varchar,
	fault5_day varchar,
	fault5_hour varchar,
	fault5_minute varchar,
	fault5_second varchar,
	fault5_datahigh varchar,
	fault5_datalow varchar,
	fault6_realtime_year varchar,
	fault6_realtime_month varchar,
	fault6_realtime_day varchar,
	fault6_realtime_hour varchar,
	fault6_realtime_minute varchar,
	fault6_realtime_second varchar,
	fault6_datahigh varchar,
	fault6_datalow varchar,
	fault7_realtime_year varchar,
	fault7_realtime_month varchar,
	fault7_realtime_day varchar,
	fault7_realtime_hour varchar,
	fault7_realtime_minute varchar,
	fault7_realtime_second varchar,
	fault7_datahigh varchar,
	fault7_datalow varchar,
	fault8_realtime_year varchar,
	fault8_realtime_month varchar,
	fault8_realtime_day varchar,
	fault8_realtime_hour varchar,
	fault8_realtime_minute varchar,
	fault8_realtime_second varchar,
	fault8_datahigh varchar,
	fault8_datalow varchar,
	fault9_realtime_year varchar,
	fault9_realtime_month varchar,
	fault9_realtime_day varchar,
	fault9_realtime_hour varchar,
	fault9_realtime_minute varchar,
	fault9_realtime_second varchar,
	fault9_datahigh varchar,
	fault9_datalow varchar,
--	is_connect boolean default false,
	bt_real_soc numeric,
	load_main_pw numeric,
	load_sub_pw numeric,
	load_main_pw_h numeric,
	load_sub_pw_h numeric,
	inverter_freq numeric,
	bt_v numeric,
	bt_i numeric,
	create_dt timestamp with time zone default now() not null,
	colec_dt timestamp with time zone not null,
    meter_v numeric,
    meter_i numeric,
    meter_active_pw numeric,
    meter_reactive_pw numeric,
    meter_pwfactor numeric,
    meter_freq numeric
) select
device_id
varchar
not null,
--    device_type_cd
varchar
not null,
oper_stus_cd
varchar,
pv_pw
numeric,
pv_pw_h
numeric,
pv_pw_co2
numeric,
pv_stus_cd
varchar,
cons_pw
numeric,
cons_pw_h
numeric,
cons_pw_co2
numeric,
bt_pw
numeric,
bt_chrg_pw_h
numeric,
bt_chrg_pw_co2
numeric,
bt_dchrg_pw_h
numeric,
bt_dchrg_pw_co2
numeric,
bt_soc
numeric,
bt_soh
numeric,
bt_stus_cd
varchar,
grid_pw
numeric,
grid_ob_pw_h
numeric,
grid_ob_pw_co2
numeric,
grid_tr_pw_h
numeric,
grid_tr_pw_co2
numeric,
grid_stus_cd
varchar,
pcs_pw
numeric,
pcs_fd_pw_h
numeric,
pcs_pch_pw_h
numeric,
ems_opmode
varchar,
pcs_opmode1
varchar,
pcs_opmode2
varchar,
pcs_tgt_pw
numeric,
feed_in_limit
varchar,
max_inverter_pw_cd
varchar,
rfsh_prd
numeric
default
60
not null,
outlet_pw
numeric,
outlet_pw_h
numeric,
outlet_pw_co2
numeric,
pwr_range_cd_ess_grid
varchar,
pwr_range_cd_grid_ess
varchar,
pwr_range_cd_grid_load
varchar,
pwr_range_cd_ess_load
varchar,
pwr_range_cd_pv_ess
varchar,
pcs_comm_err_rate
numeric,
smtr_comm_err_rate
numeric,
m2m_comm_err_rate
numeric,
pcs_flag0
varchar,
pcs_flag1
varchar,
pcs_flag2
varchar,
pcs_opmode_cmd1
varchar,
pcs_opmode_cmd2
varchar,
pv_v1
numeric,
pv_i1
numeric,
pv_pw1
numeric,
pv_v2
numeric,
pv_i2
numeric,
pv_pw2
numeric,
inverter_v
numeric,
inverter_i
numeric,
inverter_pw
numeric,
dc_link_v
numeric,
g_rly_cnt
numeric,
bat_rly_cnt
numeric,
grid_to_grid_rly_cnt
numeric,
bat_pchrg_rly_cnt
numeric,
bms_flag0
varchar,
bms_diag0
varchar,
rack_v
numeric,
rack_i
numeric,
cell_max_v
numeric,
cell_min_v
numeric,
cell_avg_v
numeric,
cell_max_t
numeric,
cell_min_t
numeric,
cell_avg_t
numeric,
tray_cnt
numeric,
smtr_tp_cd
varchar,
smtr_pulse_cnt
numeric,
smtr_modl_cd
varchar,
pv_max_pwr1
numeric,
pv_max_pwr2
numeric,
instl_rgn
varchar,
mmtr_slv_addr
numeric,
basicmode_cd
varchar,
sys_st_cd
varchar,
bt_chrg_st_cd
varchar,
bt_dchrg_st_cd
varchar,
pv_st_cd
varchar,
grid_st_cd
varchar,
smtr_st_cd
varchar,
smtr_tr_cnter
numeric,
smtr_ob_cnter
numeric,
dn_enable
varchar,
dc_start
varchar,
dc_end
varchar,
nc_start
varchar,
nc_end
varchar,
grid_code0
varchar,
grid_code1
varchar,
grid_code2
varchar,
pcon_bat_targetpower
varchar,
grid_code3
varchar,
bdc_module_code0
varchar,
bdc_module_code1
varchar,
bdc_module_code2
varchar,
bdc_module_code3
varchar,
fault5_realtime_year
varchar,
fault5_realtime_month
varchar,
fault5_day
varchar,
fault5_hour
varchar,
fault5_minute
varchar,
fault5_second
varchar,
fault5_datahigh
varchar,
fault5_datalow
varchar,
fault6_realtime_year
varchar,
fault6_realtime_month
varchar,
fault6_realtime_day
varchar,
fault6_realtime_hour
varchar,
fault6_realtime_minute
varchar,
fault6_realtime_second
varchar,
fault6_datahigh
varchar,
fault6_datalow
varchar,
fault7_realtime_year
varchar,
fault7_realtime_month
varchar,
fault7_realtime_day
varchar,
fault7_realtime_hour
varchar,
fault7_realtime_minute
varchar,
fault7_realtime_second
varchar,
fault7_datahigh
varchar,
fault7_datalow
varchar,
fault8_realtime_year
varchar,
fault8_realtime_month
varchar,
fault8_realtime_day
varchar,
fault8_realtime_hour
varchar,
fault8_realtime_minute
varchar,
fault8_realtime_second
varchar,
fault8_datahigh
varchar,
fault8_datalow
varchar,
fault9_realtime_year
varchar,
fault9_realtime_month
varchar,
fault9_realtime_day
varchar,
fault9_realtime_hour
varchar,
fault9_realtime_minute
varchar,
fault9_realtime_second
varchar,
fault9_datahigh
varchar,
fault9_datalow
varchar,
--    is_connect
boolean
default
false,
bt_real_soc
numeric,
load_main_pw
numeric,
load_sub_pw
numeric,
load_main_pw_h
numeric,
load_sub_pw_h
numeric,
inverter_freq
numeric,
bt_v
numeric,
bt_i
numeric,
create_dt
timestamp
with time zone default now() not null,
colec_dt timestamp with time zone not null,
from tb_opr_mon_state_pms_gen3_hist'''

QUERY_INSERT_INTO_GEN2_HIST='''
insert into tb_opr_mon_state_pms_gen2_hist(
	device_id varchar not null,
-- 	device_type_cd varchar not null,
	oper_stus_cd varchar,
	pv_pw numeric,
	pv_pw_h numeric,
	pv_pw_co2 numeric,
	pv_stus_cd varchar,
	cons_pw numeric,
	cons_pw_h numeric,
	cons_pw_co2 numeric,
	bt_pw numeric,
	bt_chrg_pw_h numeric,
	bt_chrg_pw_co2 numeric,
	bt_dchrg_pw_h numeric,
	bt_dchrg_pw_co2 numeric,
	bt_soc numeric,
	bt_soh numeric,
	bt_stus_cd varchar,
	grid_pw numeric,
	grid_ob_pw_h numeric,
	grid_ob_pw_co2 numeric,
	grid_tr_pw_h numeric,
	grid_tr_pw_co2 numeric,
	grid_stus_cd varchar,
	pcs_pw numeric,
	pcs_fd_pw_h numeric,
	pcs_pch_pw_h numeric,
	ems_opmode varchar,
	pcs_opmode1 varchar,
	pcs_opmode2 varchar,
	pcs_tgt_pw numeric,
	feed_in_limit varchar,
	max_inverter_pw_cd varchar,
	rfsh_prd numeric default 60 not null,
	outlet_pw numeric,
	outlet_pw_h numeric,
	outlet_pw_co2 numeric,
	pwr_range_cd_ess_grid varchar,
	pwr_range_cd_grid_ess varchar,
	pwr_range_cd_grid_load varchar,
	pwr_range_cd_ess_load varchar,
	pwr_range_cd_pv_ess varchar,
	pcs_comm_err_rate numeric,
	smtr_comm_err_rate numeric,
	m2m_comm_err_rate numeric,
	pcs_flag0 varchar,
	pcs_flag1 varchar,
	pcs_flag2 varchar,
	pcs_opmode_cmd1 varchar,
	pcs_opmode_cmd2 varchar,
	pv_v1 numeric,
	pv_i1 numeric,
	pv_pw1 numeric,
	pv_v2 numeric,
	pv_i2 numeric,
	pv_pw2 numeric,
	inverter_v numeric,
	inverter_i numeric,
	inverter_pw numeric,
	dc_link_v numeric,
	g_rly_cnt numeric,
	bat_rly_cnt numeric,
	grid_to_grid_rly_cnt numeric,
	bat_pchrg_rly_cnt numeric,
	bms_flag0 varchar,
	bms_diag0 varchar,
	rack_v numeric,
	rack_i numeric,
	cell_max_v numeric,
	cell_min_v numeric,
	cell_avg_v numeric,
	cell_max_t numeric,
	cell_min_t numeric,
	cell_avg_t numeric,
	tray_cnt numeric,
	smtr_tp_cd varchar,
	smtr_pulse_cnt numeric,
	smtr_modl_cd varchar,
	pv_max_pwr1 numeric,
	pv_max_pwr2 numeric,
	instl_rgn varchar,
	mmtr_slv_addr numeric,
	basicmode_cd varchar,
	sys_st_cd varchar,
	bt_chrg_st_cd varchar,
	bt_dchrg_st_cd varchar,
	pv_st_cd varchar,
	grid_st_cd varchar,
	smtr_st_cd varchar,
	smtr_tr_cnter numeric,
	smtr_ob_cnter numeric,
	dn_enable varchar,
	dc_start varchar,
	dc_end varchar,
	nc_start varchar,
	nc_end varchar,
	grid_code0 varchar,
	grid_code1 varchar,
	grid_code2 varchar,
	pcon_bat_targetpower varchar,
	grid_code3 varchar,
	bdc_module_code0 varchar,
	bdc_module_code1 varchar,
	bdc_module_code2 varchar,
	bdc_module_code3 varchar,
	fault5_realtime_year varchar,
	fault5_realtime_month varchar,
	fault5_day varchar,
	fault5_hour varchar,
	fault5_minute varchar,
	fault5_second varchar,
	fault5_datahigh varchar,
	fault5_datalow varchar,
	fault6_realtime_year varchar,
	fault6_realtime_month varchar,
	fault6_realtime_day varchar,
	fault6_realtime_hour varchar,
	fault6_realtime_minute varchar,
	fault6_realtime_second varchar,
	fault6_datahigh varchar,
	fault6_datalow varchar,
	fault7_realtime_year varchar,
	fault7_realtime_month varchar,
	fault7_realtime_day varchar,
	fault7_realtime_hour varchar,
	fault7_realtime_minute varchar,
	fault7_realtime_second varchar,
	fault7_datahigh varchar,
	fault7_datalow varchar,
	fault8_realtime_year varchar,
	fault8_realtime_month varchar,
	fault8_realtime_day varchar,
	fault8_realtime_hour varchar,
	fault8_realtime_minute varchar,
	fault8_realtime_second varchar,
	fault8_datahigh varchar,
	fault8_datalow varchar,
	fault9_realtime_year varchar,
	fault9_realtime_month varchar,
	fault9_realtime_day varchar,
	fault9_realtime_hour varchar,
	fault9_realtime_minute varchar,
	fault9_realtime_second varchar,
	fault9_datahigh varchar,
	fault9_datalow varchar,
--	is_connect boolean default false,
	bt_real_soc numeric,
	load_main_pw numeric,
	load_sub_pw numeric,
	load_main_pw_h numeric,
	load_sub_pw_h numeric,
	inverter_freq numeric,
	bt_v numeric,
	bt_i numeric,
	create_dt timestamp with time zone default now() not null,
	colec_dt timestamp with time zone not null,
    meter_v numeric,
    meter_i numeric,
    meter_active_pw numeric,
    meter_reactive_pw numeric,
    meter_pwfactor numeric,
    meter_freq numeric
) select
device_id
varchar
not null,
--    device_type_cd
varchar
not null,
oper_stus_cd
varchar,
pv_pw
numeric,
pv_pw_h
numeric,
pv_pw_co2
numeric,
pv_stus_cd
varchar,
cons_pw
numeric,
cons_pw_h
numeric,
cons_pw_co2
numeric,
bt_pw
numeric,
bt_chrg_pw_h
numeric,
bt_chrg_pw_co2
numeric,
bt_dchrg_pw_h
numeric,
bt_dchrg_pw_co2
numeric,
bt_soc
numeric,
bt_soh
numeric,
bt_stus_cd
varchar,
grid_pw
numeric,
grid_ob_pw_h
numeric,
grid_ob_pw_co2
numeric,
grid_tr_pw_h
numeric,
grid_tr_pw_co2
numeric,
grid_stus_cd
varchar,
pcs_pw
numeric,
pcs_fd_pw_h
numeric,
pcs_pch_pw_h
numeric,
ems_opmode
varchar,
pcs_opmode1
varchar,
pcs_opmode2
varchar,
pcs_tgt_pw
numeric,
feed_in_limit
varchar,
max_inverter_pw_cd
varchar,
rfsh_prd
numeric
default
60
not null,
outlet_pw
numeric,
outlet_pw_h
numeric,
outlet_pw_co2
numeric,
pwr_range_cd_ess_grid
varchar,
pwr_range_cd_grid_ess
varchar,
pwr_range_cd_grid_load
varchar,
pwr_range_cd_ess_load
varchar,
pwr_range_cd_pv_ess
varchar,
pcs_comm_err_rate
numeric,
smtr_comm_err_rate
numeric,
m2m_comm_err_rate
numeric,
pcs_flag0
varchar,
pcs_flag1
varchar,
pcs_flag2
varchar,
pcs_opmode_cmd1
varchar,
pcs_opmode_cmd2
varchar,
pv_v1
numeric,
pv_i1
numeric,
pv_pw1
numeric,
pv_v2
numeric,
pv_i2
numeric,
pv_pw2
numeric,
inverter_v
numeric,
inverter_i
numeric,
inverter_pw
numeric,
dc_link_v
numeric,
g_rly_cnt
numeric,
bat_rly_cnt
numeric,
grid_to_grid_rly_cnt
numeric,
bat_pchrg_rly_cnt
numeric,
bms_flag0
varchar,
bms_diag0
varchar,
rack_v
numeric,
rack_i
numeric,
cell_max_v
numeric,
cell_min_v
numeric,
cell_avg_v
numeric,
cell_max_t
numeric,
cell_min_t
numeric,
cell_avg_t
numeric,
tray_cnt
numeric,
smtr_tp_cd
varchar,
smtr_pulse_cnt
numeric,
smtr_modl_cd
varchar,
pv_max_pwr1
numeric,
pv_max_pwr2
numeric,
instl_rgn
varchar,
mmtr_slv_addr
numeric,
basicmode_cd
varchar,
sys_st_cd
varchar,
bt_chrg_st_cd
varchar,
bt_dchrg_st_cd
varchar,
pv_st_cd
varchar,
grid_st_cd
varchar,
smtr_st_cd
varchar,
smtr_tr_cnter
numeric,
smtr_ob_cnter
numeric,
dn_enable
varchar,
dc_start
varchar,
dc_end
varchar,
nc_start
varchar,
nc_end
varchar,
grid_code0
varchar,
grid_code1
varchar,
grid_code2
varchar,
pcon_bat_targetpower
varchar,
grid_code3
varchar,
bdc_module_code0
varchar,
bdc_module_code1
varchar,
bdc_module_code2
varchar,
bdc_module_code3
varchar,
fault5_realtime_year
varchar,
fault5_realtime_month
varchar,
fault5_day
varchar,
fault5_hour
varchar,
fault5_minute
varchar,
fault5_second
varchar,
fault5_datahigh
varchar,
fault5_datalow
varchar,
fault6_realtime_year
varchar,
fault6_realtime_month
varchar,
fault6_realtime_day
varchar,
fault6_realtime_hour
varchar,
fault6_realtime_minute
varchar,
fault6_realtime_second
varchar,
fault6_datahigh
varchar,
fault6_datalow
varchar,
fault7_realtime_year
varchar,
fault7_realtime_month
varchar,
fault7_realtime_day
varchar,
fault7_realtime_hour
varchar,
fault7_realtime_minute
varchar,
fault7_realtime_second
varchar,
fault7_datahigh
varchar,
fault7_datalow
varchar,
fault8_realtime_year
varchar,
fault8_realtime_month
varchar,
fault8_realtime_day
varchar,
fault8_realtime_hour
varchar,
fault8_realtime_minute
varchar,
fault8_realtime_second
varchar,
fault8_datahigh
varchar,
fault8_datalow
varchar,
fault9_realtime_year
varchar,
fault9_realtime_month
varchar,
fault9_realtime_day
varchar,
fault9_realtime_hour
varchar,
fault9_realtime_minute
varchar,
fault9_realtime_second
varchar,
fault9_datahigh
varchar,
fault9_datalow
varchar,
--    is_connect
boolean
default
false,
bt_real_soc
numeric,
load_main_pw
numeric,
load_sub_pw
numeric,
load_main_pw_h
numeric,
load_sub_pw_h
numeric,
inverter_freq
numeric,
bt_v
numeric,
bt_i
numeric,
create_dt
timestamp
with time zone default now() not null,
colec_dt timestamp with time zone not null,
from tb_opr_mon_state_pms_gen3_hist_temp'''


CHANGED_COLUMNS={'inverter_v_dc':'bt_v','inverter_i_dc':'bt_i','grid_v':'meter_v','grid_i':'grid_v',
                                            'grid_active':'meter_active_pw','grid_reactive':'meter_reactive_pw','grid_pwfactor':'meter_pwfactor',
                                            'grid_freq':'meter_freq'}

TARGET_TABLE="""
TB_OPR_MON_STATE_PMS_GEN2_HIST(device_id,
oper_stus_cd,
pv_pw,
pv_pw_h,
pv_pw_co2,
pv_stus_cd,
cons_pw,
cons_pw_h,
cons_pw_co2,
bt_pw,
bt_chrg_pw_h,
bt_chrg_pw_co2,
bt_dchrg_pw_h,
bt_dchrg_pw_co2,
bt_soc,
bt_soh,
bt_stus_cd,
grid_pw,
grid_ob_pw_h,
grid_ob_pw_co2,
grid_tr_pw_h,
grid_tr_pw_co2,
grid_stus_cd,
pcs_pw,
pcs_fd_pw_h,
pcs_pch_pw_h,
ems_opmode,
pcs_opmode1,
pcs_opmode2,
pcs_tgt_pw,
feed_in_limit,
max_inverter_pw_cd,
outlet_pw,
outlet_pw_h,
outlet_pw_co2,
pcs_comm_err_rate,
smtr_comm_err_rate,
m2m_comm_err_rate,
pcs_flag0,
pcs_flag1,
pcs_flag2,
pcs_opmode_cmd1,
pcs_opmode_cmd2,
pv_v1,
pv_i1,
pv_pw1,
pv_v2,
pv_i2,
pv_pw2,
inverter_v,
inverter_i,
inverter_pw,
dc_link_v,
g_rly_cnt,
bat_rly_cnt,
grid_to_grid_rly_cnt,
bat_pchrg_rly_cnt,
bms_flag0,
bms_diag0,
rack_v,
rack_i,
cell_max_v,
cell_min_v,
cell_avg_v,
cell_max_t,
cell_min_t,
cell_avg_t,
tray_cnt,
smtr_tp_cd,
smtr_pulse_cnt,
smtr_modl_cd,
pv_max_pwr1,
pv_max_pwr2,
instl_rgn,
mmtr_slv_addr,
basicmode_cd,
sys_st_cd,
bt_chrg_st_cd,
bt_dchrg_st_cd,
pv_st_cd,
grid_st_cd,
smtr_st_cd,
smtr_tr_cnter,
smtr_ob_cnter,
grid_code0,
grid_code1,
grid_code2,
pcon_bat_targetpower,
grid_code3,
bdc_module_code0,
bdc_module_code1,
bdc_module_code2,
bdc_module_code3,
fault5_realtime_year,
fault5_realtime_month,
fault5_day,
fault5_hour,
fault5_minute,
fault5_second,
fault5_datahigh,
fault5_datalow,
fault6_realtime_year,
fault6_realtime_month,
fault6_realtime_day,
fault6_realtime_hour,
fault6_realtime_minute,
fault6_realtime_second,
fault6_datahigh,
fault6_datalow,
fault7_realtime_year,
fault7_realtime_month,
fault7_realtime_day,
fault7_realtime_hour,
fault7_realtime_minute,
fault7_realtime_second,
fault7_datahigh,
fault7_datalow,
fault8_realtime_year,
fault8_realtime_month,
fault8_realtime_day,
fault8_realtime_hour,
fault8_realtime_minute,
fault8_realtime_second,
fault8_datahigh,
fault8_datalow,
fault9_realtime_year,
fault9_realtime_month,
fault9_realtime_day,
fault9_realtime_hour,
fault9_realtime_minute,
fault9_realtime_second,
fault9_datahigh,
fault9_datalow,
bt_real_soc,
load_main_pw,
load_sub_pw,
load_main_pw_h,
load_sub_pw_h,
inverter_freq,
create_dt,
colec_dt,
meter_v,
meter_i,
meter_active_pw,
meter_reactive_pw,
meter_pwfactor,
meter_freq)
"""