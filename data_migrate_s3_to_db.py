from util.config import  MAX_DATA_STORGE_DATE,DEVICE_TYPE_CD
from util.query import CHANGED_COLUMNS,TARGET_TABLE
from util.query import QUERY_INSERT_INTO_GEN2_TEMP,QUERY_INSERT_INTO_GEN2_HIST,CREATE_PMS_GEN2_TEMP,DROP_PMS_GEN2_TEMP
import os
import sys
import util.DBManager as DBM
from util import s3_util
import pandas as pd
import datetime
from dateutil import tz
from util.config import DEVICE_TYPE
import shutil
import logging.config
import json
import traceback




def set_logger():
    config = json.load(open('util/logger-config.json'))
    logging.config.dictConfig(config)
    global logger
    logger = logging.getLogger(__name__)


def make_directory(file_path):
    if not os.path.isdir(file_path):
        os.makedirs(file_path)


def remove_all_file(file_path):
    if os.path.exists(file_path):
        shutil.rmtree(file_path)
        logger.debug('{0} directory delete'.format(file_path))



def s3_to_db(st_date_of_migrate_data,db_manager):
    #0. field preparation
    file_name = 'ess_state_1min_data_{0}.csv'
    file_path = '/root/tmp/'
    try:

        #for i in range(MAX_DATA_STORGE_DATE):
        for i in range(1):
            start_time = datetime.datetime.utcnow()
            #0-1 field preparation
            csv_file_name=file_name.format(st_date_of_migrate_data.strftime("%Y-%m-%d"))
            #0-2 migration process start
            logger.debug('=============1 day start : {0}============'.format(st_date_of_migrate_data))
            #0-3 clean directory
            remove_all_file(file_path)
            make_directory(file_path)

            #1. start download
            s3_util.eu_download_csv_file_to_local(file_path,csv_file_name,st_date_of_migrate_data)
            print('download finish')
            #2. preprocess downloaded csv
            df_migrate_1min = pd.read_csv(file_path + csv_file_name, dtype=DEVICE_TYPE.get("pms_gen2"))
            df_migrate_1min.rename(columns=CHANGED_COLUMNS,inplace=True)
            df_migrate_1min.drop(columns=DROP_COLUMNS,inplace=True,axis=1)

            #2-2 Timestamp 처리
            if df_migrate_1min['create_dt'].dtype == 'O':
                df_migrate_1min['create_dt'] = pd.to_datetime(df_migrate_1min['create_dt'], format='%Y-%m-%d %H:%M:%S',
                                                              errors='raise', utc=True)
            else:
                df_migrate_1min['create_dt'] = pd.to_datetime(df_migrate_1min['create_dt'], format='%Y%m%d%H%M%S',
                                                              errors='raise', utc=True)

            if df_migrate_1min['colec_dt'].dtype == 'O':
                df_migrate_1min['colec_dt'] = pd.to_datetime(df_migrate_1min['colec_dt'], format='%Y%m%d%H%M%S',
                                                             errors='raise', utc=True)
            else:
                df_migrate_1min['colec_dt'] = pd.to_datetime(df_migrate_1min['colec_dt'], format='%Y-%m-%d %H:%M:%S',
                                                             errors='raise', utc=True)

            #2-3 csv 저장
            df_migrate_1min.to_csv(file_path+"result_"+csv_file_name,index=False)
            print('preprocess finish')
            #3. copy from csv to db
            db_manager.copy_csv_to_db(TARGET_TABLE,file_path+"result_"+csv_file_name)
            logger.debug('1 day end time : {0}'.format(datetime.datetime.utcnow() - start_time))
            print('db copy finish')
            st_date_of_migrate_data=st_date_of_migrate_data+datetime.timedelta(days=1)
    except Exception as e:
        logger.error(traceback.format_exc())


def migrate_data(device_type):
    # 0-0 logfile,db_manager,device_type 설정
    set_logger()
    db_manager = DBM.DBManager()
    # 0-1 날짜 설정
    start_time = datetime.datetime.utcnow()
    #st_date_of_migrate_data = start_time-datetime.timedelta(days=MAX_DATA_STORGE_DATE)
    #test
    st_date_of_migrate_data = datetime.datetime.strptime('01/01/21', '%d/%m/%y')
    # 0-2 쿼리에 넣을 날짜 문자열 생성
    st_date_trunc_to_hour=st_date_of_migrate_data.replace(hour=0,minute=0, second=0, microsecond=0)
    # 이동 실행
    logger.debug('=====start migration : start_day = {0} , start_time {1}'.format(st_date_trunc_to_hour,start_time))
    s3_to_db(st_date_trunc_to_hour,db_manager)
    logger.debug('===========migration end time : {0}'.format(datetime.datetime.utcnow() - start_time))
    logger.debug('=============end============')
    print('=====start migration : target_day = {0} '.format(datetime.datetime.utcnow() - start_time))




if __name__ == '__main__':
    migrate_data(sys.argv[1])


